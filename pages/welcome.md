# Welcome to the Climate4Impact 2.0 portal

The aim of Climate4impact is to enhance the use of climate research data. 
It is currently under development in the European project IS-ENES3. Climate4impact is connected to the [Earth System Grid Federation (ESGF) infrastructure](https://esgf.llnl.gov/mission.html) using ESGF search and thredds catalogs. 
The portal aims to support climate change impact modellers, impact and adaptation consultants, as well anyone else wanting to use climate change data. 

The sections below are just placeholders and will guide you in the future to the specific domains. 

<table class="drupal" style="width: 800px; margin-right: auto; margin-left: auto;"><tbody><tr>
<td> <!-- <a href="/impactportal/documentation/guidanceandusecases.jsp?q=agriculture_forestry"> -->
<a   href="images/agriculture_land_200.jpg">
<img src="images/agriculture_land_200.jpg"/><div class="tnimagetext"><p>Agriculture/Forestry</p></div></div></a></td>
<td> <!-- <a href="/impactportal/documentation/guidanceandusecases.jsp?q=energy"> -->
<a   href="images/energy_200.jpg">
<img src="images/energy_200.jpg"/><div class="tnimagetext"><p>Energy</p></div></div></a></td>
<td> <!-- <a href="/impactportal/documentation/guidanceandusecases.jsp?q=health"> -->
<a   href="images/health_200.jpg">
<img src="images/health_200.jpg"/><div class="tnimagetext"><p>Health</p></div></div></a></td>
<td> <!-- <a href="/impactportal/documentation/guidanceandusecases.jsp?q=infrastructure_urban"> -->
<a   href="images/infrastructure_200.jpg">
<img src="images/infrastructure_200.jpg"/><div class="tnimagetext"><p>Infrastructure/Urban</p></div></div></a></td></tr><tr>
<td> <!-- <a href="/impactportal/documentation/guidanceandusecases.jsp?q=marine_coastal"> -->
<a   href="images/coastal_200.jpg">
<img src="images/coastal_200.jpg"/><div class="tnimagetext"><p>Marine/Coastal</p></div></div></a></td>
<td> <!-- <a href="/impactportal/documentation/guidanceandusecases.jsp?q=nature_biodiversity"> -->
<a   href="images/biodiversity_200.jpg">
<img src="images/biodiversity_200.jpg"/><div class="tnimagetext"><p>Nature/Biodiversity</p></div></div></a></td>
<td> <!-- <a href="/impactportal/documentation/guidanceandusecases.jsp?q=tourism"> -->
<a   href="images/tourism_200.jpg">
<img src="images/tourism_200.jpg"/><div class="tnimagetext"><p>Tourism</p></div></div></a></td>
<td> <!-- <a href="/impactportal/documentation/guidanceandusecases.jsp?q=water_management"> -->
<a   href="images/water_management_200.jpg">
<img src="images/water_management_200.jpg"/><div class="tnimagetext"><p>Water Management</p></div></div></a></td></tr>
</tbody></table>

## Current status:

- 2019-09-01: The current version of the portal supports searching and downloading public CMIP6 datasets available on the ESGF.
