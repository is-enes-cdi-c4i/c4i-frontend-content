
# Manual for changing the content

In this repository you can create and edit the information for C4I.
The plain tekst is supposed to be stored in the 'pages' folder, the images in the 'images' folder.

## Change a page

- Go to gitlab.com and open the project "c4i-frontend-content"
- In the Web IDE, you can change the content of the .md files. Open the files you want to change and edit the text.
- To preview the markdown, , but not preview and edit at the same time.
- Markdown Cheatsheet:
  - <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>
  - <https://docs.gitlab.com/ee/user/markdown.html>
- Make sure you opened the "dev" branch, and not "master" or another branch.
- When you finished editing the text, continue with \[Commit...].
- You can now compare your changes to the old version. When you agree to the changes you made, you can stage your changes. Only the files that are staged are added to the new version. When you selected your files to be changed, you can commit.
- Add your description in the "Commit Message" and "Commit to **dev** branch". You can keep the default message, but if you customize; try to keep the message as short as possible. Gitlab prefers messages with a maximum of 50 characters (including space).  Click on \[Commit] for the third time.
  
## Discard changes

- If you are busy with a change but did not commit, you can just go back by clicking on the projects name and accept that you don't want to save.
- Another way to do it, is to finish all the files you want to commit and stage them. You can safely discard all the unstaged files.  
- You cannot undo the changes you committed completely. If you want an older version, perform a new change and undo the changes manually.

## Add an image

- You can upload a picture by clicking on the three vertical dots next to the map you want to upload them and select "Upload file".
- To use them into a file add the following template \<img src="map/picture.jpg"/>

## Add, remove or rename a page

- In the Web IDE you can add a new file (button next to "Edit") in the map pages.
- Fill in the name of the new page with the extension .md
- Do not use spaces, but use "_" for a space
- You can use the three vertical dots next to the file to delete or rename the file.

## Publishing your changes

- Test
