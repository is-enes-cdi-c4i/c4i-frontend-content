#!/bin/bash

echo  "$0: Manual upload of C4I documentation content into AWS S3 bucket"

export PATH_TO_C4I_ROOTDIR=$(realpath $(dirname $0)/../..)
#echo ${PATH_TO_C4I_ROOTDIR}
export PATH_TO_C4I_FRONTEND_CONTENT_DIR=${PATH_TO_C4I_ROOTDIR}/c4i-frontend-content

unset USING_AWS_ACCESS_KEY 
if [ -v AWS_ACCESS_KEY_ID ]; then
  if [ -v AWS_SECRET_ACCESS_KEY ]; then
    echo "AWS_ACCESS_KEY_ID - defined, AWS_SECRET_ACCESS_KEY - defined "
    export USING_AWS_ACCESS_KEY=1
  else
    echo "AWS_ACCESS_KEY_ID - defined,  AWS_SECRET_ACCESS_KEY - NOT defined "
  fi
fi

function getCONTENT_BRANCH() {
  local branch
  cd ${PATH_TO_C4I_FRONTEND_CONTENT_DIR} &&   branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null) ; cd - > /dev/null
  echo $branch
}

unset CONTENT_BRANCH
CONTENT_BRANCH=`getCONTENT_BRANCH`
if [ "$CONTENT_BRANCH" == "" ]; then
  echo "ERROR: Could not determine the active branch of the C4I_FRONTEND_CONTENT"
  if [ -d $PATH_TO_C4I_FRONTEND_CONTENT_DIR ]; then  
    echo "PATH_TO_C4I_FRONTEND_CONTENT_DIR=${PATH_TO_C4I_FRONTEND_CONTENT_DIR} : directory EXISTS"
    ls -al ${PATH_TO_C4I_FRONTEND_CONTENT_DIR}
  else
    echo "PATH_TO_C4I_FRONTEND_CONTENT_DIR=${PATH_TO_C4I_FRONTEND_CONTENT_DIR} : directory NOT exists"
  fi
  exit 1
fi
echo "PATH_TO_C4I_FRONTEND_CONTENT_DIR=${PATH_TO_C4I_FRONTEND_CONTENT_DIR}"
echo "CONTENT_BRANCH=$CONTENT_BRANCH"

if [ -v USING_AWS_ACCESS_KEY ]; then
    echo "USING ENV-VARS: AWS_ACCESS_KEY_ID + AWS_SECRET_ACCESS_KEY .."
else
  grep "\[c4i\]" ${HOME}/.aws/credentials > /dev/null 2>&1
  exitStatus=$?
  if [ "$exitStatus" != "0" ] ; then
    echo "ERROR: Missing AWS credentials"
    echo "SOLUTION A): set env. variables: AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY"
    echo "SOLUTION B): "
    echo "Create file \${HOME}/.aws/credentials"
    echo "[c4i]"
    echo "aws_access_key_id = [key_id]"
    echo "aws_secret_access_key = [access_key]"
    echo "Create a file \${HOME}/.aws/config"
    echo "Where numbers = knmi-iam identity"
    echo "[profile c4i]"
    echo "role_arn = arn:aws:iam::[numbers]:role/knmi-operator"
    echo "source_profile = default"
    echo "region = eu-west-1"
    exit 1
  fi
  echo "USING \${HOME}/.aws/credentials .."
fi

if [ -v S3_BUCKET_NAME ]; then
  echo "GIVEN S3_BUCKET_NAME=$S3_BUCKET_NAME"
else
  # Temporary S3 buckets:
  # c4i-frontend-content-master-temp
  # c4i-frontend-content-dev-temp
  # https://s3.console.aws.amazon.com/s3/buckets/c4i-frontend-content-master-temp/?region=eu-west-1&tab=overview
  # https://s3.console.aws.amazon.com/s3/buckets/c4i-frontend-content-dev-temp/?region=eu-west-1&tab=overview
  echo "Using default/temporary S3 buckets.."
  export S3_BUCKET_NAME=c4i-frontend-content-master-temp
  echo "GIVEN S3_BUCKET_NAME=$S3_BUCKET_NAME"
fi

if [ "$CONTENT_BRANCH" != "master" ]; then
  export C4I_FRONTEND_CONTENT_S3_DNS=$( echo ${S3_BUCKET_NAME} | sed "s/master/dev/g" )
else
  export C4I_FRONTEND_CONTENT_S3_DNS=${S3_BUCKET_NAME}
fi
echo "USING C4I_FRONTEND_CONTENT_S3_DNS=$C4I_FRONTEND_CONTENT_S3_DNS"

cd ${PATH_TO_C4I_FRONTEND_CONTENT_DIR}


if [ -v USING_AWS_ACCESS_KEY ]; then
  cmd="aws s3 ls ${C4I_FRONTEND_CONTENT_S3_DNS}"
  echo $cmd
  $cmd
  cmd=(aws s3 sync . s3://${C4I_FRONTEND_CONTENT_S3_DNS}/ --delete --exclude '".git/*"' --exclude ".gitlab-ci.yml" --exclude '"_*"' --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control "no-cache,no-store")
  echo ${cmd[@]}
  if [ -v DRYRUN ]; then
    echo "NOTHING executed in DRYRUN mode."
  else
    #$cmd
    #${cmd[@]}
    aws s3 sync . s3://${C4I_FRONTEND_CONTENT_S3_DNS}/ --delete --exclude ".git/*" --exclude ".gitlab-ci.yml" --exclude "_*" --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control "no-cache,no-store" 
  fi
else
  #--cache-control "no-cache, no-store"
  cmd=(aws s3 sync . s3://${C4I_FRONTEND_CONTENT_S3_DNS}/"" --delete --exclude '".git/*"' --exclude '.gitlab-ci.yml' --exclude '"_*"' --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control "no-cache,no-store" --profile c4i)
  #cmd=(aws s3 sync . s3://${C4I_FRONTEND_CONTENT_S3_DNS}/ --delete --exclude "\".git/*\"" --exclude '.gitlab-ci.yml' --exclude "\"_*\"" --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control 'no-cache,no-store' --profile c4i)
  #cmd=(aws s3 sync . s3://${C4I_FRONTEND_CONTENT_S3_DNS}/ --delete --exclude \'.git/*\' --exclude '.gitlab-ci.yml' --exclude \'_*\' --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control 'no-cache,no-store' --profile c4i)
  #cmd=(./_maintenance/printCmd.bash s3 sync . s3://${C4I_FRONTEND_CONTENT_S3_DNS}/ --delete --exclude \'.git/*\' --exclude '.gitlab-ci.yml' --exclude \'_*\' --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control 'no-cache,no-store' --profile c4i)
  echo ${cmd[@]}
  if [ -v DRYRUN ]; then
    echo "NOTHING executed in DRYRUN mode."
  else
    #$cmd
    #${cmd[@]}
    aws s3 sync . s3://${C4I_FRONTEND_CONTENT_S3_DNS}/ --delete --exclude ".git/*" --exclude ".gitlab-ci.yml" --exclude "_*" --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control "no-cache,no-store" --profile c4i
  fi
fi

